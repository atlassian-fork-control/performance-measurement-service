import { Page } from 'puppeteer';
import { capitalizeFirstLetter } from '../../utils';
import Command from './Command';

export default class QuickInsert implements Command {
  keyword: string;

  constructor(keyword?: string) {
    if (!keyword) {
      throw new Error('Required argument `keyword` for `quick_insert` is missing');
    }
    this.keyword = capitalizeFirstLetter(keyword);
  }

  async execute(page: Page) {
    await page.waitForSelector('.ProseMirror');
    await page.click(`.ProseMirror`);
    await page.type('.ProseMirror', `/${this.keyword}`);
    await page.waitForSelector('div[aria-label="Popup"]');
    await page.waitForSelector(`[aria-label="Popup"] [role="button"][aria-describedby="${this.keyword}"]`);
    await page.click(`[aria-label="Popup"] [role="button"]`);

    return {};
  }
}
