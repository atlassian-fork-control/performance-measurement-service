import * as commands from './commands';
import Command from './commands/Command';
// import { isCommand } from './commands/Command';

export default (args: any[]) => {
  if (args.length < 1) {
    throw new Error('Not possible to execute step without a command!');
  }
  const [commandName, ...rest] = args;
  const klass = (commands as any)[commandName];
  if (klass) {
    return new klass(...rest[0]) as Command;
  } else {
    // throw new Error(`Unknown command ${commandName}!`);
  }
};
