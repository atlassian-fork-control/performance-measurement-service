import { baseConfig, Config } from '@atlassian/node-config';
import { ioTsGuard } from '@atlassian/node-config-io-ts';
import * as t from 'io-ts';

/**
 * CORS Config
 */
export const CORSConfigSchema = t.exact(
  t.type({
    origin: t.string
  })
);
export type CORSConfig = t.TypeOf<typeof CORSConfigSchema>;
interface KeyedCORSConfig {
  cors: CORSConfig;
}
const corsConfig = new Config<KeyedCORSConfig>({
  guard: ioTsGuard<KeyedCORSConfig>(CORSConfigSchema, 'cors')
});

/**
 * Logger Config
 */
export const LoggerConfigSchema = t.exact(
  t.type({
    defaultLevel: t.string,
    responseTimeLimit: t.number
  })
);
export type LoggerConfig = t.TypeOf<typeof LoggerConfigSchema>;
interface KeyedLoggerConfig {
  logger: LoggerConfig;
}
const loggerConfig = new Config<KeyedLoggerConfig>({
  guard: ioTsGuard<KeyedLoggerConfig>(LoggerConfigSchema, 'logger')
});

export const configBuilder = baseConfig.concat(loggerConfig).concat(corsConfig);

export type ApplicationConfig = ReturnType<typeof configBuilder.build>;
export default configBuilder.build({
  envPrefix: 'pfpms_'
});
