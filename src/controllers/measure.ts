import { HttpError } from '@atlassian/koa-error';
import * as puppeteer from 'puppeteer';
import { mean, median, standardDeviation } from 'simple-statistics';
import { Context } from '../lib/context';
import { getPerformanceMetric, getTime, initializeBrowser, isValidPayload, PerformanceMetrics } from '../lib/utils';

import { runDsl } from '../lib/dsl';

interface Metrics {
  sd: number;
  mean: number;
  median: number;
}

const NUMBER_OF_ITERATIONS = 11;

export async function measure(ctx: Context) {
  const { length, body } = ctx.request;
  if (!length || !isValidPayload(body)) {
    throw new HttpError(400, 'Invalid payload');
  }

  const browser = await initializeBrowser();

  const { tests } = body;
  const testNames = Object.keys(tests);
  const data: { [key: string]: { time: Metrics; memory: Metrics } } = {};

  for (let k = 0, count = testNames.length; k < count; k++) {
    const testName = testNames[k];
    const steps = tests[testName];
    const times: number[] = [];
    const memory: number[] = [];

    for (let i = 0; i < NUMBER_OF_ITERATIONS; i++) {
      const page: puppeteer.Page = await browser.newPage();

      // time
      const startTime = await getTime(page);
      await runDsl(browser, page, { baseUrl: body.url }, steps);
      const endTime = await getTime(page);

      times.push(Math.round(endTime - startTime));

      // memory
      const client: puppeteer.CDPSession = await page.target().createCDPSession();
      await client.send('Performance.enable');

      const metrics = (await client.send('Performance.getMetrics')) as PerformanceMetrics;
      const JSHeapUsedSize = getPerformanceMetric('JSHeapUsedSize', metrics) || 0;

      memory.push(JSHeapUsedSize / 1048576);

      await client.detach();
      await page.close();
    }

    // Remove the first item since it's the most expensive one
    times.shift();

    data[testName] = {
      time: {
        sd: standardDeviation(times),
        mean: mean(times),
        median: median(times)
      },
      memory: {
        sd: standardDeviation(memory),
        mean: mean(memory),
        median: median(memory)
      }
    };
  }

  // await page.screenshot({ path: './test.png' });

  await browser.close();

  ctx.body = { data };
}
