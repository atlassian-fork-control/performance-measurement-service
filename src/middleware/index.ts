import * as cors from 'kcors';
import * as bodyParser from 'koa-bodyparser';
import * as compose from 'koa-compose';
import error, { HttpError } from '@atlassian/koa-error';
import logger from '@atlassian/koa-logger';
import tracer from '@atlassian/koa-tracer';
import { Logger } from '@atlassian/logger';
import Metrics from '@atlassian/metrics';
import { CORSConfig } from '../config';
import securityHeaders from './security-headers';
import wrapper from './wrapper';
import metrics from './metrics';

export interface MiddleWareOptions {
  cors: CORSConfig;
  logger: Logger;
  metrics: Metrics;
}

// default middleware
export default function(opts: MiddleWareOptions) {
  return compose([
    // wrapper for all requests
    // logs histogram metrics about the request
    wrapper(opts.metrics),

    // sets ctx.tracer (tracer instance)
    // also adds Zipkin headers to response ("X-B3-*")
    tracer(),

    // sets ctx.metrics = opts.metrics
    // ctx.metrics is used for setting custom metrics in the app
    metrics(opts.metrics),

    // sets ctx.logger (logger instance) and ctx.logRequest (boolean)
    // also sets "X-Response-Time" response header ("140ms")
    // if you want to prevent writing logs set ctx.logRequest to false
    logger(opts.logger),

    // error handling, wraps other middlewares code into try..catch
    // error thrown should better be a "HttpError" which is also provided by this module
    error(),

    bodyParser({
      onerror(err, ctx) {
        throw new HttpError(422, 'body parse error');
      }
    }),

    cors({ origin: opts.cors.origin }),

    securityHeaders
  ]);
}
