import * as Router from 'koa-router';
import * as controller from './controllers';
import { withEndpoint } from './lib/utils';

export function basic(router: Router) {
  return router.get('/health', withEndpoint(controller.health, 'healthcheck')).routes();
}

export function measure(router: Router) {
  return router.post('/measure', withEndpoint(controller.measure, 'measure')).routes();
}
